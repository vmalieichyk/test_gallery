//
//  Wrapper.swift
//  testapp
//
//  Created by Viktor Malieichyk on 10/11/18.
//  Copyright © 2018 viktor. All rights reserved.
//

import Foundation

class Wrapper: Codable {
    let photos: Photos
    let stat: String
}
