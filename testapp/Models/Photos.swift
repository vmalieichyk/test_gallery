//
//  Photos.swift
//  testapp
//
//  Created by Viktor Malieichyk on 10/11/18.
//  Copyright © 2018 viktor. All rights reserved.
//

import Foundation

class Photos: Codable {
    let page: Int
    let pages: Int
    let perpage: Int
    let total: String
    let photo: [Photo]
    
    var meta: Meta {
        return Meta(page: page, pages: pages, perpage: perpage, total: Int(total) ?? pages * perpage)
    }
}

struct Meta {
    let page: Int
    let pages: Int
    let perpage: Int
    let total: Int
}


extension Photos {
    var indexPaths: [IndexPath] {
        return (0..<photo.count).map { IndexPath(row: $0 + (self.page - 1) * perpage - (self.page - 1), section: 0) }
    }
}

