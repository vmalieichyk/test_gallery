//
//  Photo.swift
//  testapp
//
//  Created by Viktor Malieichyk on 10/11/18.
//  Copyright © 2018 viktor. All rights reserved.
//

import Foundation

class Photo: Codable {
    let id: String
    let owner: String
    let secret: String
    let server: String
    let farm: Int
    let title: String
}


extension Photo {
    var imageURL: URL? {
        let urlString = "https://farm\(farm).static.flickr.com/\(server)/\(id)_\(secret).jpg"
        return URL(string: urlString)
    }
}
