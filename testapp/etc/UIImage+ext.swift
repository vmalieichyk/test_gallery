//
//  UIImage+ext.swift
//  testapp
//
//  Created by Viktor Malieichyk on 10/11/18.
//  Copyright © 2018 viktor. All rights reserved.
//

import UIKit

extension UIColor {
    open class var random: UIColor {
        let randomRed:CGFloat = CGFloat(drand48())
        let randomGreen:CGFloat = CGFloat(drand48())
        let randomBlue:CGFloat = CGFloat(drand48())
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
    }
}
