//
//  APIClient.swift
//  testapp
//
//  Created by Viktor Malieichyk on 10/11/18.
//  Copyright © 2018 viktor. All rights reserved.
//

import Foundation


enum APIResponse<Value> {
    case success(Value)
    case failure(Error)
}

struct APIError: Error {
    let statusCode: Int
}

class APIClient {
    class func getPhotos(page: UInt = 1, completion: @escaping (APIResponse<Photos>) -> ()) {
        let url = URL(string:"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=77ac1e6b3640da6aa59f6133d7b00f8b&format=json&nojsoncallback=1&tags=pet&page=\(page)")!

        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: url) { data, response, error in
            if let error = error {
                completion(APIResponse.failure(error))
                return
            }
            guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                    let statusCode = (response as? HTTPURLResponse)!.statusCode

                    DispatchQueue.main.async {
                        completion(.failure(APIError(statusCode: statusCode)))
                    }
                    return
            }
            if let data = data {
                
                if let wrapper = try? JSONDecoder().decode(Wrapper.self, from: data) {
                    DispatchQueue.main.async {
                        completion(APIResponse.success(wrapper.photos))
                    }
                    return
                }
            }
            DispatchQueue.main.async {
                completion(.failure(APIError(statusCode: -1)))
            }
        }
        
        task.resume()
    }
}
