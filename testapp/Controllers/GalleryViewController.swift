//
//  ViewController.swift
//  testapp
//
//  Created by Viktor Malieichyk on 10/7/18.
//  Copyright © 2018 viktor. All rights reserved.
//

import UIKit

enum GalleryState {
    case empty
    case loading
    case populated(photos: [Photo], meta: Meta)
    case loadingMore(photos: [Photo], meta: Meta)
    
    var count: Int {
        switch self {
        case .populated(let photos, _):
            return photos.count
        case .loadingMore(let photos, _):
            return photos.count
        default:
            return 0
        }
    }
    
    var total: Int {
        switch self {
        case .populated(_, let meta):
            return meta.total
        case .loadingMore(_, let meta):
            return meta.total
        default:
            return 0
        }
    }
    
    var photos: [Photo] {
        switch self {
        case .populated(let photos, _):
            return photos
        case .loadingMore(let photos, _):
            return photos
        default:
            return []
        }
    }
    
    var nextPage: UInt {
        switch self {
        case .populated(_, let meta):
            return UInt(min(meta.page + 1, meta.pages))
        default:
            return 1
        }
    }
}

class GalleryViewController: UICollectionViewController {
    private let refreshControl = UIRefreshControl()
    
    var state: GalleryState = .empty {
        didSet {

        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.collectionView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)

        
        loadPhotos()
    }
    
    @objc func refresh(_ sender: Any?) {
        loadPhotos()
    }
    
    func loadPhotos(page: UInt = 1) {
        switch state {
        case .empty:
            self.state = .loading
        case .populated(let photos, let meta):
            self.state = .loadingMore(photos: photos, meta: meta)
        case .loading:
            return
        case .loadingMore:
            return
        }
        
        APIClient.getPhotos(page: page) { [weak self] response in
            guard self != nil else { return }
            
            switch response {
            case .success(let result):
                self?.collectionView.performBatchUpdates({
                    if result.page == 1 {
                        self!.state = .populated(photos: result.photo, meta: result.meta)
                    } else {
                        self!.state = .populated(photos: result.photo + self!.state.photos, meta: result.meta)
                    }
                    self?.collectionView.insertItems(at: result.indexPaths)
                }, completion: { (end) in
                    
                })

                
            case .failure(let error):
                print(error)
            }
        }
    }
}

// MARK: DataSource
extension GalleryViewController {
    
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return state.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch self.state {
        case .populated:
            if indexPath.row > self.state.count - 30 {
                self.loadPhotos(page: self.state.nextPage)
            }
        default:
            break
        }

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as! GalleryCell

        //for debug
//        if let label = cell.viewWithTag(14312) as? UILabel {
//            label.text = "\(indexPath.row)"
//        } else {
//            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 20))
//            label.tag = 14312
//            label.text = "\(indexPath.row)"
//            cell.addSubview(label)
//        }
        let photo = self.state.photos[indexPath.row]
        
        //Nuke library
        if let url = photo.imageURL {
            loadImage(with: url, into: cell.imageView)
        }
        
        cell.backgroundColor = .random
        
        return cell
    }
}


// MARK: Layout Delegate
let columns: CGFloat = 3
fileprivate let insets = UIEdgeInsets(top: 20, left: 10, bottom: 20, right: 10)

extension GalleryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.frame.width - insets.left * (columns + 1)
        let cellWidth = floor(width / columns)
        
        return CGSize(width: cellWidth, height: cellWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return insets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return insets.left
    }
}
