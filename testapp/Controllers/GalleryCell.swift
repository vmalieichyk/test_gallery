//
//  GalleryCell.swift
//  testapp
//
//  Created by Viktor Malieichyk on 10/11/18.
//  Copyright © 2018 viktor. All rights reserved.
//

import UIKit

class GalleryCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    
    override func prepareForReuse() {
        self.imageView.image = nil
    }
}
